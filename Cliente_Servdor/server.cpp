  /* Server code in C */
 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <thread>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

vector<string> names;
vector<int> sockets;


void new_client(int ConnectFD, int SocketFD)
{
    if(0 > ConnectFD)
    {
        perror("error accept failed");
        close(SocketFD);
        exit(EXIT_FAILURE);
    }
    int n;
    char buffer[256];
    while(1)
    {
     bzero(buffer,256);
     n = read(ConnectFD,buffer,256);
     if (n < 0) perror("ERROR reading from socket");
     printf("Here is the message: [%s]\n",buffer);
     n = write(ConnectFD,"I got your message",18);
     if (n < 0) perror("ERROR writing to socket");
     /* perform read write operations ... */
      
    }
    shutdown(ConnectFD, SHUT_RDWR);
    close(ConnectFD);
}

 
int main()
{
    struct sockaddr_in stSockAddr;
    int SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    char buffer[256];
    int n;
 
    if(-1 == SocketFD)
    {
      perror("can not create socket");
      exit(EXIT_FAILURE);
    }
 
    memset(&stSockAddr, 0, sizeof(struct sockaddr_in));
 
    stSockAddr.sin_family = AF_INET;
    stSockAddr.sin_port = htons(1100);
    stSockAddr.sin_addr.s_addr = INADDR_ANY;
 
    if(-1 == bind(SocketFD,(const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in)))
    {
        perror("error bind failed");
        close(SocketFD);
        exit(EXIT_FAILURE);
    }
 
    if(-1 == listen(SocketFD, 10))
    {
        perror("error listen failed");
        close(SocketFD);
        exit(EXIT_FAILURE);
    }

    vector<thread> clients;
    while(1)
    {
        int ConnectFD = accept(SocketFD, NULL, NULL);
        sockets.push_back(ConnectFD);
        clients.push_back(thread(new_client,ConnectFD, SocketFD));
    }
    for (auto& th : clients) th.join();

 
    close(SocketFD);
    return 0;
}
